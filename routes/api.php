<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register');
Route::post('logout', 'App\Http\Controllers\Auth\LogoutController@logout')->middleware('auth:api');
Route::get('user', 'App\Http\Controllers\Auth\UserController@User')->middleware('auth:api');
Route::post('forgot', 'App\Http\Controllers\Auth\ForgotPassword@forgot');
Route::post('sample','App\Http\Controllers\Auth\LoginController@GetRefreshToken');