<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ForgotPassword extends Controller
{
    
    public function forgot(Request $request){

        $email = $request->email;

        if(User::where('email', $email)->doesntExist()){

            return response([
                
                'message' => 'User not doesn\'t exists!'
                
            ], 404);
        }

        $token = Str::random(10);

        DB::table('password_resets')->insert([

            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()

        ]);
    }

}
