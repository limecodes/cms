<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use URL;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request){
        
        try {
           
            $user = User::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'confirm_password' => $request->confirm_password
            ]);
            
            return response([
                
                'message' => 'Successfully registerd',
                'status' => 200

            ], 201);
            
        } catch (\Exception $e) {
            
            return response([
            
                'message' => $e->getMessage()
            
            ], 400);

        }

    }
}
