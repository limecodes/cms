<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login(Request $request){

        if(Auth::attempt($request->only('email','password'))){

            $user = Auth::user();

            $token = $user->createToken('app')->accessToken;

            return response([

                'message' => 'success',
                'token' => $token,
                'user' => $user,

            ], 200);

        }

        return response([

            'message' => 'Invalid username & password'

        ], 401);
    }

}
